package tn.esprit.ioc.reflection;

import java.util.logging.Logger;
import java.lang.reflect.Field;

//A Lire : https://dzone.com/articles/java-dynamic-proxy
public class Main {

	public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
		HelloReflection privateObject = new HelloReflection("The Private Value");

		Field privateStringField = HelloReflection.class.getDeclaredField("privateString");
		privateStringField.setAccessible(true);

		String fieldValue = (String) privateStringField.get(privateObject);
		Logger.getAnonymousLogger().info("fieldValue =  " + fieldValue);
	}
}
